FROM mcr.microsoft.com/mssql/server:2017-CU12

# Set environment variables
ENV ACCEPT_EULA=Y
ENV PATH_JAR=/install/liquibase-4.5.0/mssql-jdbc.jar
ENV URL=jdbc:sqlserver://localhost:1433;encrypt=false
ENV SA_USER=sa
ENV SA_PASSWORD=YourStrong!Passw0rd

# Copy Liquibase files
RUN mkdir /install
RUN mkdir /data
COPY data.zip /data/data.zip
COPY liquibase-4.5.0.zip /install/liquibase-4.5.0.zip

# Install unzip utility and Java
RUN apt-get update && apt-get install -y unzip default-jre	

# Unzip changes files
RUN unzip /install/liquibase-4.5.0.zip -d /install
RUN unzip /data/data.zip -d /data

RUN ls /data

# Apply Liquibase changes
RUN /opt/mssql/bin/sqlservr --accept-eula & (echo "awaiting server bootup" && sleep 15 && echo "lets try to logon"  && java -jar /install/liquibase-4.5.0/liquibase.jar --changeLogFile=/data/changelog.xml \
    --classpath=$PATH_JAR \
    --url=$URL \
    --username=$SA_USER \
    --password=$SA_PASSWORD \
    update)

# Expose SQL Server port
EXPOSE 1433